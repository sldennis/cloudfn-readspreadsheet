
var readSpreadsheet = require('./readSpreadsheet.js')

exports.getSpreadsheet = (req, res) => {
	var spreadsheetId = req.body.spreadsheetId;
	var range = req.body.range;
	var credentialsPath = req.body.credentialsPath;
	var apiKeyPath = req.body.apiKeyPath;

	readSpreadsheet(spreadsheetId, range, credentialsPath, apiKeyPath, function(data){
		res.status(200).send(JSON.stringify(data));
	});
};

