# cloudfn-getSpreadsheet

Returns a json array of the data in the specified google spreadsheet worksheet.

## Parameters

Pass all the below parameters as json in the req object.

**spreadsheetId**: the id of the google spreadsheet
**range**: worksheet and range you want to retrieve in A1 notation
**credentialsPath**: the path to the file containing the credentials, see details below for more details
**apiKeyPath**: the path to the file containing the api key

## Credentials Path


This is the credentials used to access the google spreadsheet. Go to GCP > IAM & admin > IAM to create a service account and download the credentials.

Format:
{
  "type": "service_account",
  "project_id": "*gcp project id*",
  "private_key_id": "*private key id*",
  "private_key": "*private key*",
  "client_email": "*client email*",
  "client_id": "*client id*",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://accounts.google.com/o/oauth2/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/spreadsheet-reader%40solvay-wcc-gcp-sandbox-prod.iam.gserviceaccount.com"
}

## API Key
This is the api key to use google sheet api in GCP. Can be found in GCP > API & Services > Credentials. 

Please enable google sheet api in GCP > API & Services > Google Sheets API

Format: 
{
  "key": "*api key*"
}

## Requirement

Google SDK, NodeJs, NPM

## Installation

run `npm install`

## Deploy to google cloud

`gcloud beta functions deploy getSpreadsheet --runtime nodejs8 --trigger http`

## Calling the spreadsheet

`curl -X POST "https://YOUR_REGION-YOUR_PROJECT_ID.cloudfunctions.net/getSpreadsheet" 
-H "Content-Type:application/json" 
--data 
'{
    "spreadsheetId":"SPREADSHEET_ID",
    "range":"RANGE",
    "credentialsPath":"PATH_TO_SERVICE_ACCOUNT_CREDENTIALS",
    "apiKeyPath":"PATH_TO_API_KEY_TO_USE_GOOGLE_SPREADSHEETS_API" 
}'
`