
const {google} = require('googleapis');
var sheets = google.sheets('v4');
var readSpreadsheet = require('./readSpreadsheet.js')

const spreadsheetId = "1QhY8-yBK-myl3VqukezjgSMdPOnCYzhEUCTPSJz2hqI";
const range = "EHS_Data";
const credentialsPath = "./service_account_credentials.json";
const apiKeyPath = "./api_key.json"

readSpreadsheet(spreadsheetId, range, credentialsPath, apiKeyPath, function(data){
	console.log(data);
});