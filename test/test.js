var expect = require('chai').expect;
var readSpreadsheet = require('../readSpreadsheet.js');

const spreadsheetId = "1QhY8-yBK-myl3VqukezjgSMdPOnCYzhEUCTPSJz2hqI";
const range = "EHS_Data";
const credentialsPath = "./service_account_credentials.json";
const apiKeyPath = "./api_key.json";

describe("Read Spreadsheet", function(){
	describe("Read specific spreadsheet as json", function(){
		it("should not be empty", function(){
			readSpreadsheet(spreadsheetId, range, credentialsPath, apiKeyPath, function(data){
				expect(data).to.not.be.null;
			});
		});
	});
});