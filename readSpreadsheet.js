const {google} = require('googleapis');
var sheets = google.sheets('v4');

function readSpreadsheet(spreadsheetId, range, credentialsPath, apiKeyPath, callback){

	var credentials = require(credentialsPath);
	var jwt = new google.auth.JWT(
    			credentials.client_email, 
    			null, 
    			credentials.private_key,
    			['https://www.googleapis.com/auth/spreadsheets.readonly']
  	);

  	var apiKeyFile = require(apiKeyPath);
	var apiKey = apiKeyFile.key

	var data = null;

	var request = {
		spreadsheetId: spreadsheetId,
		range: range,
		auth: jwt,
		key: apiKey,
		valueRenderOption: 'UNFORMATTED_VALUE', 
	};

	sheets.spreadsheets.values.get(request, function(err, response) {
		if (err) {
			console.error(err);
			return;
		}

		if(response != null){
			data = response.data;
			var formattedData = convertSpreadsheetJson(data, 'values');
			callback(formattedData);
		} else {
			console.log("Empty response within get")
			callback("");
		}
	});
}

function populate(data){
	var obj = {};
	for(i=0;i<this.length;i++){
		try{
			obj[this[i]] = data[i];	
		}catch(err){}
	}	
	return obj;	
}

function convertSpreadsheetJson(sheet, KEY_VALUES){
	try{
		if(sheet[KEY_VALUES] && sheet[KEY_VALUES].length > 1){
			var data = sheet[KEY_VALUES];
			var keys = data.shift();
			var result = data.map(populate, keys);

			return result;
		}
	}catch(err){
		console.log(err);
	}
}

module.exports = readSpreadsheet;